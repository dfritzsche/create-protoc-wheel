import os

import protoc


def main():
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    protoc.exec_protoc(["protoc", "-I.", "-otest.desc", "test.proto"])


if __name__ == "__main__":
    main()
